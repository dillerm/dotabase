#!/usr/bin/env python3.5

from distutils.core import setup

setup(name='dotabase',
      version='1.0',
      description='Dota sqlite database access',
      author='Malcolm Diller',
      author_email='gward@python.net',
      url='http://dotabase.me',
      packages=['dotabase'],
     )